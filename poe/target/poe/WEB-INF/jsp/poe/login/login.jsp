<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <title></title>
	<link rel="stylesheet" href="<c:url value='/'/>css/login.css" />
	<script src="<c:url value='/'/>js/jquery-1.11.3.min.js"></script>
	<script src="<c:url value='/'/>js/jquery-ui.min.js"></script>
	<script>
		$(document).ready(function () {
			//container height
			$("#container").css("height", $("#wrap").height());

			$(".btn_reference").click(function(){
				$("#reference").toggleClass("on");
			});
		});

	</script>
	<script type="text/javascript">
	<!--
	function actionLogin() {
	    if (document.loginForm.user_id.value =="") {
	        alert("아이디를 입력하세요");
	        return false;
	    } else if (document.loginForm.user_pwd.value =="") {
	        alert("비밀번호를 입력하세요");
	        return false;
	    } else {
	        document.loginForm.action="<c:url value='/login/loginAction.do'/>";
	        //document.loginForm.j_username.value = document.loginForm.userSe.value + document.loginForm.username.value;
	        //document.loginForm.action="<c:url value='/j_spring_security_check'/>";
	        document.loginForm.submit();
	    }
	}
	/* 
	function setCookie (name, value, expires) {
	    document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString();
	}
	
	function getCookie(Name) {
	    var search = Name + "="
	    if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
	        offset = document.cookie.indexOf(search)
	        if (offset != -1) { // 쿠키가 존재하면
	            offset += search.length
	            // set index of beginning of value
	            end = document.cookie.indexOf(";", offset)
	            // 쿠키 값의 마지막 위치 인덱스 번호 설정
	            if (end == -1)
	                end = document.cookie.length
	            return unescape(document.cookie.substring(offset, end))
	        }
	    }
	    return "";
	}
	
	function saveid(form) {
	    var expdate = new Date();
	    // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
	    if (form.checkId.checked)
	        expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30); // 30일
	    else
	        expdate.setTime(expdate.getTime() - 1); // 쿠키 삭제조건
	    setCookie("saveid", form.id.value, expdate);
	}
	
	function getid(form) {
	    form.checkId.checked = ((form.id.value = getCookie("saveid")) != "");
	}
	
	function fnInit() {
	    var message = document.loginForm.message.value;
	    if (message != "") {
	        alert(message);
	    }
	    getid(document.loginForm);
	}
	 */
	//-->
	</script>
</head>
<body><!--   onload="fnInit();" -->
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    
<!-- 전체 레이어 시작 -->

<div id="wrap" class="login">
	<header>
		<div id="header">
			<h1><img src="<c:url value='/'/>images/login_logo.png" alt="2016 해안쓰레기 일제조사" /></h1>
		</div>
	</header>
	<div id="container">
		<form class="content" name="loginForm" method="post" >
			<fieldset>
				<label for="user_id"><span class="blind">ID</span></label><input type="text" name="user_id" id="user_id" class="" placeholder="ID" />
				<label for="user_pwd"><span class="blind">Password</span></label><input type="password" name="user_pwd" id="user_pwd" class="" placeholder="Password" />
				<a href="#" class="btn_login" onclick="javascript:actionLogin()">Login</a>
			</fieldset>
		</form>
	</div>
	<footer>
		<div id="footer">
			<p class="copyright">Copyright&copy;. 2016. KOEM. All Rights Reserved.</p>
		</div>
	</footer>
</div>
<!-- //전체 레이어 끝 -->
</body>
</html>
