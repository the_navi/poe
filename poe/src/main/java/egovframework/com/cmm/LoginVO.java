package egovframework.com.cmm;

import java.io.Serializable;

public class LoginVO implements Serializable{

	private static final long serialVersionUID = -8274004534207618049L;
	
	private String user_id;
	private String user_pwd;
	private String user_name;
	private String user_institution;
	private String user_group;
	private String regist_date;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_pwd() {
		return user_pwd;
	}
	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_institution() {
		return user_institution;
	}
	public void setUser_institution(String user_institution) {
		this.user_institution = user_institution;
	}
	public String getUser_group() {
		return user_group;
	}
	public void setUser_group(String user_group) {
		this.user_group = user_group;
	}
	public String getRegist_date() {
		return regist_date;
	}
	public void setRegist_date(String regist_date) {
		this.regist_date = regist_date;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "LoginVO [user_id=" + user_id + ", user_pwd=" + user_pwd
				+ ", user_name=" + user_name + ", user_institution="
				+ user_institution + ", user_group=" + user_group
				+ ", regist_date=" + regist_date + "]";
	}
}
