package egovframework.poe.enquiry.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.poe.enquiry.service.EnquiryService;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Service("enquiryService")
public class EnquiryServiceImpl extends EgovAbstractServiceImpl implements EnquiryService {

	@Resource(name = "enquiryDAO")
	private EnquiryDAO enquiryDAO;
	
	@Override
	public EgovMap selectEnquiry(EnquiryVO paramVO) throws Exception {
		return enquiryDAO.selectEnquiry(paramVO);
	}

}