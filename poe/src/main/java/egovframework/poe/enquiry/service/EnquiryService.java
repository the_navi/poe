package egovframework.poe.enquiry.service;

import egovframework.poe.enquiry.service.impl.EnquiryVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;


public interface EnquiryService {

	public EgovMap selectEnquiry(EnquiryVO paramVO) throws Exception;

}