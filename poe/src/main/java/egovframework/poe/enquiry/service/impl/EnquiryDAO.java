package egovframework.poe.enquiry.service.impl;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Repository("enquiryDAO")
public class EnquiryDAO extends EgovAbstractDAO {

	public EgovMap selectEnquiry(EnquiryVO paramVO) {
		return (EgovMap) select("enquiryDAO.selectEnquiry", paramVO);
	}

}
