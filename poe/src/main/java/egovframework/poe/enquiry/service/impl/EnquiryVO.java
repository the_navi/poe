package egovframework.poe.enquiry.service.impl;

import java.io.Serializable;

public class EnquiryVO implements Serializable{

	private static final long serialVersionUID = -8274004534207618049L;
	
	private String userId; ;
	private String enquiryId ;
	private String sidoName ;
	private String registDate ;
	private String sidoSn ;
	private String sggName ;
	private String longitude ;
	private String latitude;
	private String originalPlaceName ;
	
	public String getEnquiryId() {
		return enquiryId;
	}
	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}
	public String getSidoName() {
		return sidoName;
	}
	public void setSidoName(String sidoName) {
		this.sidoName = sidoName;
	}
	public String getRegistDate() {
		return registDate;
	}
	public void setRegistDate(String registDate) {
		this.registDate = registDate;
	}
	public String getSidoSn() {
		return sidoSn;
	}
	public void setSidoSn(String sidoSn) {
		this.sidoSn = sidoSn;
	}
	public String getSggName() {
		return sggName;
	}
	public void setSggName(String sggName) {
		this.sggName = sggName;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getOriginalPlaceName() {
		return originalPlaceName;
	}
	public void setOriginalPlaceName(String originalPlaceName) {
		this.originalPlaceName = originalPlaceName;
	}
	
	@Override
	public String toString() {
		return "EnquiryVO [enquiryId=" + enquiryId + ", sidoName=" + sidoName
				+ ", registDate=" + registDate + ", sidoSn=" + sidoSn
				+ ", sggName=" + sggName + ", longitude=" + longitude
				+ ", latitude=" + latitude + ", originalPlaceName="
				+ originalPlaceName + "]";
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
