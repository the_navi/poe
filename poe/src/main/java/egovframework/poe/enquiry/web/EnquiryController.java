package egovframework.poe.enquiry.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.ComDefaultVO;
import egovframework.com.cmm.LoginVO;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.poe.enquiry.service.EnquiryService;
import egovframework.poe.enquiry.service.impl.EnquiryResultVO;
import egovframework.poe.enquiry.service.impl.EnquiryVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Controller@SessionAttributes(types = ComDefaultVO.class)
public class EnquiryController {

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertyService;

    @Resource(name = "EgovFileMngUtil")
    private EgovFileMngUtil fileUtil;

	@Resource(name = "enquiryService")
	private EnquiryService enquiryService;

	@RequestMapping(value = "/enquiry/goRegist.do")
	public String goRegist(@ModelAttribute("paramVO")EnquiryVO paramVO, HttpServletRequest request, ModelMap model) throws Exception {
		
		// 사용자 정보
		LoginVO loginVO = (LoginVO) EgovUserDetailsHelper.getAuthenticatedUser();
		model.addAttribute("userInfo", loginVO);
		
		// 조사 대상 정보
		EgovMap enquiryInfo = enquiryService.selectEnquiry(paramVO);
		model.addAttribute("enquiryInfo", enquiryInfo);
		
		System.out.println(enquiryInfo.toString());
		
//		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
		return "poe/enquiry/regist";
	}

	@RequestMapping(value = "/enquiry/goModify.do")
	public String goModify(@ModelAttribute("paramVO")EnquiryVO paramVO, HttpServletRequest request, ModelMap model) throws Exception {
		
//		List<?> enquiryList = enquiryService.selectEnquiry(paramVO);
//		
//		System.out.println(enquiryList.toString());
		
//		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
		return "poe/enquiry/modoify";
	}

	@RequestMapping(value = "/enquiry/goView.do")
	public String goView(@ModelAttribute("paramVO")EnquiryVO paramVO, HttpServletRequest request, ModelMap model) throws Exception {
		
		LoginVO user = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
		paramVO.setUserId(user.getUser_id());
		
		EgovMap enquiryInfo = enquiryService.selectEnquiry(paramVO);
		
		model.addAttribute("enquiryInfo", enquiryInfo);
		
		return "poe/enquiry/view";
	}

	@RequestMapping(value = "/enquiry/registAction.do")
	public String registAction(final MultipartHttpServletRequest multiRequest, @ModelAttribute("enquiryResultVO") EnquiryResultVO enquiryResultVO, HttpServletRequest request, ModelMap model) throws Exception {

		// result 저장
		LoginVO user = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

	    List<FileVO> result = null;
	    String atchFileId = "";

	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if (!files.isEmpty()) {
		result = fileUtil.parseFileInf(files, "BBS_", 0, "aaa", "");
		//atchFileId = fileMngService.insertFileInfs(result);
	    }

//		if (resultVO != null && resultVO.getUser_id() != null && !resultVO.getUser_id().equals("") && loginPolicyYn) {
//
//			request.getSession().setAttribute("LoginVO", resultVO);
//			return "redirect:/enquiry/goRegist.do?enquiryId=41570_72";
//		} else {
//
//			model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
//			return "/login/goLogin.do";
//		}
		
		return "/enquiry/goRegist.do";
	}
	public void fileUpload(MultipartFile file, MultipartHttpServletRequest request) {
	   InputStream inStream = null;    
	   OutputStream outStream = null;
	   File newFile = null;
	   try {
		   inStream = file.getInputStream();
		
		   File filePath = new File("c:/upload");
			if (!filePath.isDirectory()){
				filePath.mkdirs();
			}
		   
		   newFile = new File("c:/upload"+"/"+file.getOriginalFilename());  
		   if (!newFile.exists()) {  
			   newFile.createNewFile();  
		   }
		   outStream = new FileOutputStream(newFile);
		   int read = 0;  
		   byte[] bytes = new byte[1024];  
		   while ((read = inStream.read(bytes)) != -1) {  
			    outStream.write(bytes, 0, read);  
		   }  
	   } catch (IOException e) {
			e.printStackTrace();
			if (newFile != null && newFile.exists()) {  
				newFile.delete();
		    }	
	   } finally {
		   if(inStream != null)
			try {
				inStream.close();
				outStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	   }
	   
	}

}
