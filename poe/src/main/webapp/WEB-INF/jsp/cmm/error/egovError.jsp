<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <title></title>
	<link rel="stylesheet" href="<c:url value='/'/>css/common.css" />
	<script src="<c:url value='/'/>js/jquery-1.11.3.min.js"></script>
	<script src="<c:url value='/'/>js/jquery-ui.min.js"></script>
	<script>
		$(document).ready(function () {
			//container height
			$("#container").css("height", $("#wrap").height());
		});
		
		function fncGoAfterErrorPage(){
		    history.back(-2);
		}
	</script>	
</head>
<body>
<div id="wrap" class="error">
	<div id="container">
		<h1><span class="error_icon">Error</span>알 수 없는 오류가 발생했습니다.</h1>
		<p>담당자에게 문의 바랍니다.<a href="#LINK" onClick="fncGoAfterErrorPage();">뒤로</a></p>
	</div>
</div>
</body>
</html>
