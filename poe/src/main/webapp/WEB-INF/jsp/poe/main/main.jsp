<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="ko" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<title></title>
<link rel="stylesheet" href="<c:url value='/'/>css/common.css" />
<script src="<c:url value='/'/>js/jquery-1.11.3.min.js"></script>
<script src="<c:url value='/'/>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://map.vworld.kr/js/vworldMapInit.js.do?apiKey=767B7ADF-10BA-3D86-AB7E-02816B5B92E9"></script> 
<script>
	$(document).ready(function () {
		//container height
		$("#container").css("height", $("#wrap").height());
		$(".my_work_area .pop_bg").click(function(){
			$(".my_work_area.popup").hide();
		}); 
		
		$('#poi_search_word').keypress(function(e) { 
			if(e.which==13) { 
				doSearch(); 
				return false; 
			} 
		});
		
		getMapViewAction();
	});
</script>
<script type="text/javascript">
var apiMap;//2D map
var SOPPlugin;//3D map
vworld.showMode = false;//브이월드 배경지도 설정 컨트롤 유무(true:배경지도를 컨트롤 할수 있는 버튼 생성/false:버튼 해제)
var mControl;//마커이벤트변수
var tempMarker = null;//임시마커

/**
 * - rootDiv, mapType, mapFunc, 3D initCall, 3D failCall
 * - 브이월드 5가지 파라미터를 셋팅하여 지도 호출
 */
vworld.init("vMap", "map-first", 
	function() {        
		apiMap = this.vmap;//브이월드맵 apiMap에 셋팅 
		apiMap.setBaseLayer(apiMap.vworldBaseMap);//기본맵 설정 
		apiMap.setControlsType({"simpleMap":true});	//간단한 화면
		apiMap.addVWORLDControl("zoomBar");//panzoombar등록
		apiMap.setCenterAndZoom(14243425.793355, 4342305.8698004, 7);//화면중심점과 레벨로 이동 (초기 화면중심점과 레벨) 	
	},
	function (obj){//3D initCall(성공)
		SOPPlugin = obj;
	},
	function (msg){//3D failCall(실패)
		alert(msg);
	}
);

function getMapViewAction(){
	
	$.ajaxSetup({
		async : true
	});
	
	var param = $("#condition").serialize();
	
	var jsonURL = "/main/mainMapView.do";
	//loading();
	$.ajax({
		type : "POST",
		data : param,
		dataType : "json",
		url : jsonURL,
		beforeSend : function(){
			
		}
	}).success(function(data){
		//console.log(data);
		if(data != ""){
		    for (var i = 0, length = data.length; i < length; i++) {
		        var map_data = data[i];
		        setMarker(map_data);
		    }
		    
		    //명칭검색에서  이동 위치 선택 하면 위치로 이동시킨다.
		    var move_latitude = "${moveLatitude}";
		    var move_longitude = "${moveLongitude}";
		    alert(move_latitude);
		    if(move_latitude != "" && move_longitude != ""){
		    	moveMapCenter(".my_work_area.popup", move_latitude, move_longitude);
		    }
		    
	    }
		//jsondata = data;
	})
	.complete(function(){
		
	}).error(function(error) {
		console.log(error);
	});
	
}


/**
 * 제주도 이동 및 마커찍기
 */
function setMarker(map_data){
	
	var my_job_yn = "N";
	var icon_img = "";
	var enquiry_date_text = "미조사";
	var enquiry_status_text = "미조사";
	var enquiry_status_btn = "";
	if("${userId }" == map_data.userId){
		my_job_yn = "Y";
	}
	
	if(my_job_yn == "Y" ){
		if(map_data.enquiryStatus == "1"){
			icon_img = "<c:url value='/'/>images/marker/my_work_end_20x20.png";
			enquiry_date_text = map_data.enquiryDate;
			enquiry_status_text = "조사완료";
			enquiry_status_btn = "<a href='#' class='btn_circle view my'>more..</a>";
		}else{
			icon_img = "<c:url value='/'/>images/marker/my_work_ing_20x20.png";
			enquiry_status_btn = "<a href='#' class='btn_circle insert'>입력</a>";
		}
		
	}else{
		if(map_data.enquiryStatus == "1"){
			icon_img = "<c:url value='/'/>images/marker/other_end_20x20.png";
			enquiry_date_text = map_data.enquiryDate;
			enquiry_status_text = "조사완료";
			enquiry_status_btn = "<a href='#' class='btn_circle view'>more..</a>";
		}else{
			icon_img = "<c:url value='/'/>images/marker/other_ing_20x20.png";
		}
	}
	var popupContentHTML = "";
	popupContentHTML += "<div class='pin_box'>";
	popupContentHTML += "<dl>";
	popupContentHTML += "<dt>일련번호 : "+map_data.sidoSn+"</dt>";
	popupContentHTML += "<dd>조사일시 : "+enquiry_date_text+"</dd>";// 조사 완료 시 날짜로 변경
	popupContentHTML += "<dd>조사상태 : "+enquiry_status_text;      // 조사 완료 시 날짜로 변경
	popupContentHTML += enquiry_status_btn;      // 조사 완료 시 날짜로 변경
	//popupContentHTML += "<a href='#' class='btn_circle insert'>입력</a>";
	//popupContentHTML += "<a href='#' class='btn_circle insert other'>다른 사람 작업 전</a>";
	//popupContentHTML += "<a href='#' class='btn_circle view my'>내 작업 결과 보기</a>";
	//popupContentHTML += "<a href='#' class='btn_circle view'>타인 작업 결과 보기</a>";
	popupContentHTML += "</dl>";
	popupContentHTML += "</dl>";
	popupContentHTML += "</div>";
	
	//해당좌표로 지도 이동 및 말풍선 내용 표기

	//좌표변환
	var p4, p9;
	p4 = new OpenLayers.Projection('EPSG:4326');
	p9 = new OpenLayers.Projection('EPSG:900913');

	v = new OpenLayers.LonLat(map_data.latitude,map_data.longitude);
	p4_lat = v.lat;
	p4_lon = v.lon;

	v.transform(p4, p9);
	//v.transform(p9, p4);

    addMarker(v.lon, v.lat,  popupContentHTML, icon_img);
}
 	 
function addMarker(lon, lat, message, imgurl){
	var marker = new vworld.Marker(lon, lat,message,"");
	
	// 마커 아이콘 이미지 파일명 설정합니다.
	if (typeof imgurl == 'string') {marker.setIconImage(imgurl);}
	
	// 마커의 z-Index 설정
	marker.setZindex(3);
	marker.events.register('touchend', mControl, function(evt) {setPopup(lon, lat, message); OpenLayers.Event.stop(evt); });
	apiMap.addMarker(marker);
	tempMarker = marker; 
	
}

/**
 * 말풍선이벤트
 */
/*function mClick(lon, lat, message){
	apiMap.init();//나의 맵 초기화	
	addMarker(lon, lat,message, null);//말풍선	
}*/

function setPopup(lon, lat, message){

	var popup = new OpenLayers.Popup("chicken",
	            new OpenLayers.LonLat(lon, lat),
	            new OpenLayers.Size(200,100),
	            message,
	            true);

	apiMap.addPopup(popup);	
} 

function moveMapCenter(tag_id, latitude, longitude){
	displayChange(tag_id);
	//해당좌표로 지도 이동 및 레벨 이동
	//좌표변환
	var p4, p9;
	p4 = new OpenLayers.Projection('EPSG:4326');
	p9 = new OpenLayers.Projection('EPSG:900913');

	v = new OpenLayers.LonLat(latitude,longitude);
	p4_lat = v.lat;
	p4_lon = v.lon;

	v.transform(p4, p9);
	//v.transform(p9, p4);

	apiMap.setCenterAndZoom(v.lon,v.lat, 13); 
}


function displayChange(tag_id){
	var obj = jQuery(tag_id);
	
	if(obj.css("display")!="none"){
		obj.hide();
	}else{
		obj.show();
	}
	
}

function doSearch(){
	document.selectPoiForm.action = "<c:url value='/main/selectPoi.do'/>";
   	document.selectPoiForm.submit();
}
</script>
</head>
<body>
<form id="condition" name="condition" method="post"></form>

	<input type="hidden" name='q' id='mousex' value="0" maxlength="20" style="ime-mode:active"/>
	<input type="hidden" name='q' id='mousey' value="0" maxlength="20" style="ime-mode:active"/>
	<div id="wrap" class="gray_wrap">
		<header>
			<div id="header">
				<h1>2016 해안쓰레기 일제조사</h1>
				<ul class="header_btns">
					<li><a href="#" class="img_btns btn_mywork" onclick='javascript:displayChange(".my_work_area.popup");'>나의 메뉴</a></li>
					<li><a href="work_insert.html" class="img_btns btn_manual">메뉴얼</a></li> <!-- onclick="javascript:moveMarker();"  -->
				</ul>
			</div>
		</header>
		<div id="container">
			<!-- Map Area -->
			<div id="map" class="map_view">
				<!-- Search Area -->
				<div class="search_area">
				<form id="selectPoiForm" name="selectPoiForm" method="post">
					<label for="popupSearch"><span class="blind">지도 검색</span><input class="search_form" id="poi_search_word" name="poi_search_word" type="search" placeholder="검색"></label>
					<button type="button" class="btn_search" onclick="javascript:doSearch();">Search</button>
				</form>
				</div>
				<div id="vMap" class="map_view_area"></div>
			</div>
		</div>
	</div>

	<div class="my_work_area popup">
		<h3><em>${userId }</em> <strong>${myEnquiryList.size() }</strong>건</h3>
		<ul class="my_work_list">
			<c:if test="${not empty myEnquiryList  }">
			<c:forEach var="list" items="${myEnquiryList}" varStatus="status">
				<li onclick="javascript:moveMapCenter('.my_work_area.popup','${list.latitude}','${list.longitude}');">
					<dl>
						<dt>${list.sidoSn}</dt>
						<c:if test="${list.enquiryStatus != '1' }">
							<dd>미입력</dd>'
							<a href="#" id="pin_view" class="btn_circle insert">입력</a>
						</c:if>
						<c:if test="${list.enquiryStatus == '1' }">
							<dd>${list.enquiryDate }</dd>
							<a href="#" class="btn_circle view my">보기</a>
						</c:if>
					</dl>
				</li>
				
			</c:forEach>
			</c:if>
			
		</ul>
		<div class="pop_bg"></div>
	</div>
</body>
</html>