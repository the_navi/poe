<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="ko" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<title></title>
	<link rel="stylesheet" href="<c:url value='/'/>css/common.css" />
	<script src="<c:url value='/'/>js/jquery-1.11.3.min.js"></script>
	<script src="<c:url value='/'/>js/jquery-ui.min.js"></script>
	<script>
		$(document).ready(function () {
			//container height
			$("#container").css("height", $("#wrap").height());

			$(".btn_reference").click(function(){
				$("#reference").toggleClass("on");
			});
			
			// 실제 위치 조회
			getLocation();
			
			// 실제 위치 주소
			
			// 조사지점과 실제위치의 거리
			$("input[id=enquiryErrorDistance]").val(getDistance('${enquiryInfo.latitude}', '${enquiryInfo.longitude}', $("span[id=enquiryLatitude]").html(), $("span[id=enquiryLongitude]").html()));
			$("span[id=enquiryErrorDistance]").html(getDistance('${enquiryInfo.latitude}', '${enquiryInfo.longitude}', $("span[id=enquiryLatitude]").html(), $("span[id=enquiryLongitude]").html()).replace(/(\d)(?=(?:\d{3})+(?!\d))/g,'$1,')+"m");
		});
		
		function saveEnquiry() {
			
		}

		function getLocation() {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(showPosition, showError);
			} else {
				alert("Geolocation is not supported by this browser.");
			}
		}
		function showPosition(position) {
			$("input[id=enquiryLatitude]").val(position.coords.latitude);
			$("input[id=enquiryLongitude]").val(position.coords.longitude);
			$("span[id=enquiryLatitude]").html(position.coords.latitude);
			$("span[id=enquiryLongitude]").html(position.coords.longitude);
		}
		function showError(error) {
			switch (error.code) {
			case error.PERMISSION_DENIED:
				alert("User denied the request for Geolocation.");
				break;
			case error.POSITION_UNAVAILABLE:
				alert("Location information is unavailable.");
				break;
			case error.TIMEOUT:
				alert("The request to get user location timed out.");
				break;
			case error.UNKNOWN_ERROR:
				alert("An unknown error occurred.");
				break;
			}
		}
		
		function getDistance(lat1, lon1, lat2, lon2, unit) {
			var radlat1 = Math.PI * lat1 / 180
			var radlat2 = Math.PI * lat2 / 180
			var radlon1 = Math.PI * lon1 / 180
			var radlon2 = Math.PI * lon2 / 180
			var theta = lon1 - lon2
			var radtheta = Math.PI * theta / 180
			var dist = Math.sin(radlat1) * Math.sin(radlat2)
					+ Math.cos(radlat1) * Math.cos(radlat2)
					* Math.cos(radtheta);
			dist = Math.acos(dist)
			dist = dist * 180 / Math.PI
			dist = dist * 60 * 1.1515
			if (unit == "K") {
				dist = dist * 1.609344;		// 단위 mile 에서 km 변환.
			}
			if (unit == "N") {
				dist = dist * 0.8684;
			}
			if (unit == "M") {
				dist = dist * 1.609344 * 1000;
			}
			return dist.toFixed(2);
		}
	</script>
</head>
<body>
	<input type="hidden" name='q' id='mousex' value="0" maxlength="20" style="ime-mode:active"/>
	<input type="hidden" name='q' id='mousey' value="0" maxlength="20" style="ime-mode:active"/>
	<div id="wrap">
	<p id="geoLocation" style="display:none;">User denied the request for Geolocation.</p>
	<form id="euquiryForm" name="euquiryForm" method="post" enctype="multipart/form-data" >
	<input type="hidden" id="enquiryId" name="enquiryId" value="${enquiryInfo.enquiryId}" />
	<input type="hidden" id="enquiryErrorDistance" name="enquiryErrorDistance" value="" />
	<input type="hidden" id="enquiryLatitude" name="enquiryLatitude" value="" />
	<input type="hidden" id="enquiryLongitude" name="enquiryLongitude" value="" />
	<input type="hidden" id="enquiryAddress" name="enquiryAddress" value="" />
		<header>
			<div id="header">
				<h1>${enquiryInfo.sidoSn}</h1>
				<ul class="header_btns">
					<li><a href="index.html" class="img_btns btn_prev">홈으로</a></li>
					<li><a href="#" class="img_btns btn_save" onclick="javascript:saveEnquiry();">저장</a></li>
				</ul>
			</div>
		</header>
		<div id="container">
			<p>현재 위치와 조사대상 위치간의 거리는 <span id="enquiryErrorDistance">0m</span> 입니다</p>
			<div class="insert_card">
				<h3>조사지역</h3>
				<ul>
					<li>
						<strong>일련번호</strong>
						<div><span>${enquiryInfo.sidoSn}</span></div>
					</li>
					<li class="w50">
						<strong>위도</strong>
						<div><span id="enquiryLatitude"></span></div>
					</li>
					<li class="w50">
						<strong>경도</strong>
						<div><span id="enquiryLongitude"></span></div>
					</li>
					<li>
						<strong>주소</strong>
						<div><span id="enquiryAddress"></span></div>
					</li>
					<li>
						<strong>고유지역명</strong>
						<div><input type="text"  id="originalPlaceName" name="originalPlaceName" placeholder="입력" /></div>
					</li>
				</ul>
			</div>
			<div class="insert_card">
				<h3>조사자</h3>
				<ul>
					<li>
						<strong>소속</strong>
						<div><span>${userInfo.user_institution}</span></div>
					</li>
					<li>
						<strong>성명</strong>
						<div><span>${userInfo.user_name}</span></div>
					</li>
					<li>
						<strong>조사일시</strong>
						<div><span id="enquiryDate"></span></div>
					</li>
				</ul>
			</div>
			<div class="insert_card">
				<h3>오염도 레벨 평가</h3>
				<ul>
					<li>
						<strong>오염도 레벨</strong>
						<div>
							<select id="referanceLevel" name="referanceLevel">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
							</select>
							<button type="button" class="btn_reference">참고 이미지</button>
							<div class="reference" id="reference">
								<button type="button" class="btn_reference">닫기</button>
								<ul class="image_list">
									<li><img src="<c:url value='/'/>images/levels/level0_1.jpg" /></li>
									<li><img src="<c:url value='/'/>images/levels/level0_2.jpg" /></li>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<strong>100m 구간 해안의 평균 폭</strong>
						<div class="beach_size"><input type="number" id="seasideWidthAvg" name="seasideWidthAvg" /> m</div>
					</li>
					<li>
						<strong>주요 쓰레기 종류<span>우선순위</span></strong>
						<div>
							<ul class="check_list">
								<li>
									<input type="checkbox" id="wastesTypeFishingGearYn" name="wastesTypeFishingGearYn" value="Y" /><label for="wastesTypeFishingGearYn">페어구</label>
									<select id="wastesTypeFishingGearLevel" name="wastesTypeFishingGearLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesTypeResidentialYn" name="wastesTypeResidentialYn" value="Y" /><label for="wastesTypeResidentialYn">생활쓰레기</label>
									<select id="wastesTypeResidentialLevel" name="wastesTypeResidentialLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesTypeDrumpYn" name="wastesTypeDrumpYn" value="Y" /><label for="wastesTypeDrumpYn">투기</label>
									<select id="wastesTypeDrumpLevel" name="wastesTypeDrumpLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesTypeForeignYn" name="wastesTypeForeignYn" value="Y" /><label for="wastesTypeForeignYn">외국쓰레기</label>
									<select id="wastesTypeForeignLevel" name="wastesTypeForeignLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesTypeEtcYn" name="wastesTypeEtcYn" value="Y" /><label for="wastesTypeEtcYn">기타</label>
									<input type="text" id="wastesTypeEtcDesc" name="wastesTypeEtcDesc" />
									<select id="wastesTypeEtcLevel" name="wastesTypeEtcLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<strong>주요 쓰레기 발생원<span>우선순위</span></strong>
						<div class="check_list">
							<ul>
								<li>
									<input type="checkbox" id="wastesSourceFarmlandYn" name="wastesSourceFarmlandYn" value="Y" /><label for="wastesSourceFarmlandYn">주변 양식장</label>
									<select id="wastesSourceFarmlandLevel" name="wastesSourceFarmlandLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesSourceResidentYn" name="wastesSourceResidentYn" value="Y" /><label for="wastesSourceResidentYn">마을 주민</label>
									<select id="wastesSourceResidentLevel" name="wastesSourceResidentLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesSourceRiverYn" name="wastesSourceRiverYn" value="Y" /><label for="wastesSourceRiverYn">하천 유입</label>
									<select id="wastesSourceRiverLevel" name="wastesSourceRiverLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesSourceEtcYn" name="wastesSourceEtcYn" value="Y" /><label for="wastesSourceEtcYn">기타</label>
									<input type="text" id="wastesSourceEtcDesc" name="wastesSourceEtcDesc"/>
									<select id="wastesSourceEtcLevel" name="wastesSourceEtcLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
									</select>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<strong>배후지<span>우선순위</span></strong>
						<div class="check_list">
							<ul>
								<li>
									<input type="checkbox" id="wastesHinterFarmYn" name="wastesHinterFarmYn" value="Y" /><label for="wastesHinterFarmYn">농경지</label>
									<select id="wastesHinterFarmLevel" name="wastesHinterFarmLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesHinterVillageYn" name="wastesHinterVillageYn" value="Y" /><label for="wastesHinterVillageYn">마을</label>
									<select id="wastesHinterVillageLevel" name="wastesHinterVillageLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesHinterBeachYn" name="wastesHinterBeachYn" value="Y" /><label for="wastesHinterBeachYn">해수욕장</label>
									<select id="wastesHinterBeachLevel" name="wastesHinterBeachLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesHinterBusinessYn" name="wastesHinterBusinessYn" value="Y" /><label for="wastesHinterBusinessYn">상업</label>
									<select id="wastesHinterBusinessLevel" name="wastesHinterBusinessLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesHinterIndustryYn" name="wastesHinterIndustryYn" value="Y" /><label for="wastesHinterIndustryYn">공업</label>
									<select id="wastesHinterIndustryLevel" name="wastesHinterIndustryLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesHinterFarmYn" name="wastesHinterFarmYn" value="Y" /><label for="wastesHinterFarmYn">양식장</label>
									<select id="wastesHinterFarmLevel" name="wastesHinterFarmLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
									</select>
								</li>
								<li>
									<input type="checkbox" id="wastesHinterEtcYn" name="wastesHinterEtcYn" value="Y" /><label for="wastesHinterEtcYn">기타</label>
									<input type="text" id="wastesHinterEtcDesc" name="wastesHinterEtcDesc" />
									<select id="wastesHinterEtcLevel" name="wastesHinterEtcLevel">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
									</select>
								</li>
							</ul>
						</div>
					</li>
					<li>
						<strong>사진첨부</strong>
						<div>
							<ul class="file_list">
								<li><label for="photo1">전경사진 1</label><input type="file" name="photo1" accept="image/*" id="photo1">
								<div class="file_image"><img src="<c:url value='/'/>images/sample.jpg" /></div></li>
								<li><label for="photo2">전경사진 2</label><input type="file" name="photo2" accept="image/*" id="photo2" /></li>
								<li><label for="photo3">대표사진</label><input type="file" name="photo3" accept="image/*" id="photo3" /></li>
								<li><label for="photo4">참고사진</label><input type="file" name="photo4" accept="image/*" id="photo4" /></li>
							</ul>
						</div>
					</li>
					<li>
						<strong>수정사유</strong>
						<div><input type="text" placeholder="입력" id="editReason" name="editReason"/></div>
					</li>
				</ul>
			</div>
		</div>
	</form>
	</div>
</body>
</html>