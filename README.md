# README #

KOEM : 2016년도 해양 쓰레기 일제조사를 위한 긴급 WEB-APP 개발.

### What is this repository for? ###

* KOEM 2016년도 해양 쓰레기 일제조사를 위한 긴급 WEB-APP의 개발 소스
* Version : 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up : BitBucket 기본 설정에 준하여 이클립스를 통해 소스를 다운받는다. 
* Configuration : 없음
* Dependencies : 
* Database configuration : Oracle 11g 기준
* How to run tests : Tomcat 기준으로 테스트를 진행한다. 
* Deployment instructions : 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact